// never == nunca para
const widthoutEnd = () => {
  while (true) {
    console.log('Never');
  }
};

const fail = (message: string) => {
  throw new Error(message);
};

const example = (input: unknown) => {
  if (typeof input === 'string') {
    return 'String';
  } else if (Array.isArray(input)) {
    return 'Array';
  }
  return fail('Unknown type');
};

console.log(example('nico'));
console.log(example([]));
console.log(example(1));

console.log(example('nico'));
console.log(example([]));
