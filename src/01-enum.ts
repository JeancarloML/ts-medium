export enum ROLES {
  ADMIN = 'ADMIN',
  USER = 'USER',
  OWNER = 'OWNER',
}

export type User = {
  username: string;
  role: ROLES;
};

const nicoUser: User = {
  username: 'nico',
  role: ROLES.ADMIN,
};
