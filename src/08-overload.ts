function parseStr(input: string | string[]): string | string[] {
  if (Array.isArray(input)) {
    return input.join('');
  } else {
    return input.split('');
  }
}

const rtaArray = parseStr('abc');
// rtaArray.reverse();
if (Array.isArray(rtaArray)) {
  rtaArray.reverse();
}
console.log('🚀 ~ file: 08-overload.ts ~ line 10 ~ rtaArray', rtaArray);

const rtaStr = parseStr(['a', 'b', 'c']);
// rtaStr.toLowerCase();
if (typeof rtaStr === 'string') {
  rtaStr.toLowerCase();
}
console.log('🚀 ~ file: 08-overload.ts ~ line 14 ~ rtaStr', rtaStr);
