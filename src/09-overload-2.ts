export function parseStr(input: string): string[];
export function parseStr(input: string[]): string;
export function parseStr(input: number): boolean;

export function parseStr(input: unknown): unknown {
  if (Array.isArray(input)) {
    return input.join('');
  } else if (typeof input === 'string') {
    return input.split('');
  }
}

const rtaArray = parseStr('abc');
rtaArray.reverse();
/* if (Array.isArray(rtaArray)) {
  rtaArray.reverse();
} */
console.log('🚀 ~ file: 08-overload.ts ~ line 10 ~ rtaArray', rtaArray);

const rtaStr = parseStr(['a', 'b', 'c']);
rtaStr.toLowerCase();
/* if (typeof rtaStr === 'string') {
  rtaStr.toLowerCase();
} */
console.log('🚀 ~ file: 08-overload.ts ~ line 14 ~ rtaStr', rtaStr);

// Errores comunes de sobrecarga
// Tener varias funciones que devulevan lo mismo, pero con diferentes tipos de datos.
// Si el tipo de cambio de retorno es diferente segun parametros, es recomendable
