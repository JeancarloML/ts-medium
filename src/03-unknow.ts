let anyVar: any = 'nico';
anyVar = 1;
anyVar = true;
anyVar = {};

let isNew: boolean = anyVar;

anyVar.doSomething();
anyVar.touppercase();

let unknownVar: unknown;
unknownVar = 'nico';
unknownVar = 1;
unknownVar = true;
unknownVar = {};

// unknownVar.doSomething();
if (typeof unknownVar === 'string') {
  unknownVar.toUpperCase();
}
if (typeof unknownVar === 'boolean') {
  let isNewV2: boolean = unknownVar;
}

const parse = (value: string): unknown => {
  return JSON.parse(value);
};
