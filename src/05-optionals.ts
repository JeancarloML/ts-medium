export const createProduct = (
  id: string | number,
  isNew?: boolean,
  stock?: number
) => {
  return {
    id,
    /* stock: stock || 0,
    isNew: isNew || true, */
    // Nullish coalescing
    stock: stock ?? 0,
    isNew: isNew ?? true,
  };
};

// 0 === false
// '' === false
// false === false

const p1 = createProduct(1, true, 10);
const p2 = createProduct(2);

console.log(p1);
console.log(p2);
