import {
  addProduct,
  products,
  updateProduct,
  findProducts,
} from './products/product.service';
import { Sizes } from './products/product.model';
import faker from '@faker-js/faker';

for (let i = 0; i < 50; i++) {
  addProduct({
    description: faker.commerce.productDescription(),
    image: faker.image.imageUrl(),
    color: faker.commerce.color(),
    sizes: faker.random.arrayElement<Sizes>(['S', 'M', 'L']),
    price: parseInt(faker.commerce.price()),
    isNew: faker.datatype.boolean(),
    tags: faker.random.arrayElements(),
    title: faker.commerce.productName(),
    stock: faker.datatype.number({ min: 10, max: 100 }),
    categoryId: faker.datatype.uuid(),
  });
}

console.log(products);

const product = products[0];
updateProduct(product.id, { title: 'New Title' });

findProducts({
  stock: 10,
  color: 'red',
  tags: ['tag1', 'tag2'],
});
