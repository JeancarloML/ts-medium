import { BaseModel } from '../base.model';
export enum ROLES {
  ADMIN = 'ADMIN',
  USER = 'USER',
  OWNER = 'OWNER',
}

export interface User extends BaseModel {
  username: string;
  role: ROLES;
}
