import { Category } from '../categories/category.model';
import { BaseModel } from '../base.model';

export type Sizes = 'S' | 'M' | 'L';

export interface Product extends BaseModel {
  category: Category;
  color: string;
  description: string;
  image: string;
  isNew: boolean;
  price: number;
  sizes?: Sizes;
  stock: number;
  tags: string[];
  title: string;
}

/* type CreateProductDto = Omit<
  Product,
  'id' | 'createdAt' | 'updatedAt' | 'category'
>; */
