import { Product } from './product.model';
import {
  CreateProductDto,
  UpdatedProductDto,
  FindProduct,
} from './product.dto';
import faker from '@faker-js/faker';

export const products: Product[] = [];

export const addProduct = (data: CreateProductDto): Product => {
  const newProduct = {
    ...data,
    id: faker.datatype.uuid(),
    createdAt: new Date(),
    updatedAt: new Date(),
    category: {
      id: data.categoryId,
      name: faker.commerce.department(),
      createdAt: new Date(),
      updatedAt: new Date(),
    },
  };
  products.push(newProduct);
  return newProduct;
};

export const updateProduct = (
  id: Product['id'],
  changes: UpdatedProductDto
): Product => {
  const index = products.findIndex((product) => product.id === id);
  const prevData = products[index];
  products[index] = {
    ...prevData,
    ...changes,
  };
  return products[index];
};

export const findProducts = (dto: FindProduct): Product[] => {
  // dto.tags = ["ds"]
  // dto.tags.push('ds');
  return products;
};
