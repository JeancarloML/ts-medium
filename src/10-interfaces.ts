type Sizes = 'S' | 'M' | 'L';

/* type Product = {
  id: string | number;
  title: string;
  createdAt: Date;
  stock: number;
  sizes?: Sizes;
}; */

interface Product {
  id: string | number;
  title: string;
  createdAt: Date;
  stock: number;
  sizes?: Sizes;
}

const products: Product[] = [];

products.push({
  id: 'p1',
  title: 'Red Scarf',
  createdAt: new Date(),
  stock: 12,
  sizes: 'M',
});

const addProduct = (data: Product): void => {
  products.push(data);
};
